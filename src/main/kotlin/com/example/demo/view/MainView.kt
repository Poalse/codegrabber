package com.example.demo.view

import javafx.beans.property.SimpleStringProperty
import javafx.scene.control.Alert
import javafx.stage.FileChooser
import tornadofx.*
import java.io.File
import java.io.FileNotFoundException

class MainView : View("Code Grabber") {
    private val projectFolderInput = SimpleStringProperty()
    private val fileExtensions = SimpleStringProperty()

    override val root = pane {
        style {
            prefWidth = 320.px
            prefHeight = 195.px
        }
        vbox(spacing = 0) {
            form {
                fieldset {
                    label("Select project directory:") {
                        style {
                            fontSize = 14.px
                        }
                    }
                    hbox(spacing = 5) {
                        textfield(projectFolderInput) {
                            style {
                                prefWidth = 230.px
                                fontSize = 14.px
                            }
                        }
                        button("Select") {
                            style {
                                fontSize = 14.px
                            }
                            action {
                                val path = chooseDirectory("Select project directory: ")
                                if (path != null) {
                                    projectFolderInput.set(path.absolutePath)
                                }
                            }
                        }
                    }
                }
            }

            form {
                fieldset {
                    vbox(spacing = 5) {
                        label("Set file extensions: ") {
                            style {
                                fontSize = 14.px
                            }
                        }
                        textfield(fileExtensions) {
                            promptText = "cpp, java, py, txt, xml, ..."
                            style {
                                prefWidth = 250.px
                                fontSize = 14.px
                            }
                        }
                        button("Export") {
                            style {
                                fontSize = 14.px
                                prefWidth = 300.px
                            }
                            action {
                                if (projectFolderInput.value == null) {
                                    warningAlert("Warning!", "Select project folder!")
                                    return@action
                                }

                                val projectFolder = try {
                                    File(projectFolderInput.value)
                                } catch (e: FileNotFoundException) {
                                    warningAlert("Warning!", "Project folder not found!")
                                    return@action
                                }

                                if (fileExtensions.value == null) {
                                    warningAlert("Warning!", "Set file extensions!")
                                    return@action
                                }

                                val extensions = fileExtensions.value.split(",").map { ext -> ext.replace(" ", "") }

                                val outputFile = chooseFile(
                                    title = "Select output file: ",
                                    filters = arrayOf(FileChooser.ExtensionFilter("Text", ".txt")),
                                    mode = FileChooserMode.Save
                                ).first()

                                if (!outputFile.exists()) outputFile.createNewFile()


                                val resultText = StringBuilder()
                                fun listFilesForFolder(folder: File) {
                                    folder.listFiles()?.forEach { file ->
                                        if (file.isDirectory) listFilesForFolder(file)
                                        else {
                                            if (extensions.contains(file.extension)) {
                                                resultText.append("${file.name}\n\n")
                                                resultText.append("${file.readText()}\n\n\n\n")
                                            }
                                        }
                                    }
                                }

                                listFilesForFolder(projectFolder)
                                outputFile.writeText(resultText.toString())

                                warningAlert("Success!", "Code successful grabbed!")
                            }
                        }
                    }
                }
            }
        }
    }
}


private fun warningAlert(title: String, message: String) {
    alert(
        type = Alert.AlertType.WARNING,
        title = title,
        header = "",
        content = message
    )
}
