package com.example.demo.app

import com.example.demo.view.MainView
import com.sun.javafx.tk.Toolkit
import javafx.scene.image.Image
import javafx.stage.Stage
import tornadofx.App
import tornadofx.launch

class CodeGrabber: App(MainView::class) {
    override fun start(stage: Stage) {
        stage.isResizable = false
        stage.icons.add(Image(CodeGrabber::class.java.getResourceAsStream("/icon.png")));

        super.start(stage)
    }
}

fun main(args: Array<String>) {
    launch<CodeGrabber>(args)
}
